package com.assignment.mrlau.qrcode_scanner;

public class Coding {

    public Long _id;
    public String name;
    public String type;


    public Coding() {
        this.name = "noName";
        this.type = "noType";
    }

    public Coding(String name, String type) {
        this.name = name;
        this.type = type;
    }

}
